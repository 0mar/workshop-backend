const express = require('express');
const router  = express.Router();

//referencing workshop controller and model
var Workshops = require('../models/index');
const workshopController = require('../controllers/workshopController')


//referencing student controller and model
var Students = require('../models/index');
const studentController = require('../controllers/studentController')


router.get('/workshops', workshopController.all); //gets all workshops
router.get('/workshops/:id', workshopController.byId); //gets a specific workshop by its id
router.post('/workshops', workshopController.create); //creates a workshop
router.put('/workshops/:id', workshopController.update); // used when updating a current existing workshop
router.delete('/workshops/:id', workshopController.remove); //deletes a workshop

router.get('/students', studentController.all); //gets all students
router.get('/students/:studentID', studentController.byId); //gets a specific student by its id
router.post('/students', studentController.create); //creates a student
router.put('/students/:studentID', studentController.update); // used when updating a current existing workshop
router.delete('/students/:studentID', studentController.remove); //deletes a student

module.exports = router;

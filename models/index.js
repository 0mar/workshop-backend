const mongoose = require('mongoose');
const Schema   = mongoose.Schema,
      model    = mongoose.model.bind(mongoose),
      ObjectId = mongoose.Schema.Types.ObjectId;


//defines the workshop schema
const workshopSchema = new Schema({
  name: {
    type: String,
    unique: true,
  },
  member: String,
  location: String,
  max: Number,
});

//defines the student schema
const studentSchema = new Schema({
  studentID: Number,
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  rankings: [
    {
      ref: {type: mongoose.Schema.Types.ObjectId, ref: 'Workshops'},
      rank: Number
    }
  ]
});


// Export Mongoose model
//export default mongoose.model('workshops', workshopSchema);
const Workshops      = model('Workshops', workshopSchema);
const Students      = model('Students', studentSchema);
module.exports = {Workshops, Students};

var express    = require('express');
var logger     = require('morgan');
var path       = require('path');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');

var router     = require('./routes/index')

mongoose.connect('mongodb://127.0.0.1')

// initialize http server
const app = express();

// CORS
app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

// Handle / route
app.get('/', (req, res) =>
  res.send('Hello World!')
)

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/v1', router) //uses v1 as prefix for all API endpoints

// Launch the server on port 3000
const server = app.listen(3000, () => {
  const { address, port } = server.address();
  console.log(`Listening at http://${address}:${port}`);
});

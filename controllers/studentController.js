const Model = require('../models');
const {Students} = Model;

const studentController = {
  all (req, res) {
  //Returns all students
  Students.find({})
    .exec((err, students) => res.json(students))
  },
  byId(req, res) {
    const idParam = req.params.studentID
    Students
      .findOne({studentID: idParam})
      .exec( (err, students) => res.json(students));
  },
  create (req, res) {
    const requestBody = req.body; //creates a new record
    const newStudent = new Students(requestBody);
    //saves record to the database
    newStudent.save( (err, saved) => {
      res.send(requestBody);
    })
  },
  update (req, res) {
    const idParam = req.params.studentID;
    const reqRankings = req.body.rankings;
    const stdRankings = []


    for (let rank of reqRankings) {
      stdRankings.push({ ref: rank.workshop._id, rank: rank.ranking})
    }
    //Finds the student so it can be updated
    Students.findOneAndUpdate({studentID: idParam}, {rankings: stdRankings}, (err, doc) => {
       if(err) { console.log('Error logging data', err); }
       res.send(doc);
    });
  },
  remove (req, res) {
    const idParam = req.params.studentID;
    //Removes a workshop
    Students.findOne({studentID: idParam}).remove( (err, removed) => res.json(idParam) )
  }
};

module.exports = studentController;

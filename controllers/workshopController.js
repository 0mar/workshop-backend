const Model = require('../models');
const {Workshops} = Model;

const workshopController = {
  all (req, res) {
  //Returns all workshops
  Workshops.find({})
    .exec((err, workshops) => res.json(workshops))
  },
  byId(req, res) {
    const idParam = req.params.id;
    Workshops
      .findOne({_id: idParam})
      .exec( (err, workshops) => res.json(workshops));
  },
  create (req, res) {
    const requestBody = req.body; //creates a new record
    const newWorkshop = new Workshops(requestBody);
    //saves record to the database
    newWorkshop.save( (err, saved) => {
      res.send(requestBody);
    })
  },
  update (req, res) {
    const idParam = req.params.id;
    const workshop = req.body;
    //Finds the workshop so it can be updated
    Workshops.findOneAndUpdate({_id: idParam}, workshop, (err, doc) => {
       if(err) { console.log('Error logging data', err); }
       res.send(doc);
    });
  },
  remove (req, res) {
    const idParam = req.params.id;
    //Removes a workshop
    Workshops.findOne({_id: idParam}).remove( (err, removed) => res.json(idParam) )
  }
};

module.exports = workshopController;

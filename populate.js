import mongoose from 'mongoose';
import Workshop from './models/workshops';

const workshops = [
  {
    name: 'Hockey',
    staffMember: 'Connor Rilett',
    location: 'Camp 1',
    max: 20
  },
];


mongoose.connect('mongodb://localhost/workshops');
workshops.map(data => {
  //Initialize model with workshop data
  const workshop = new Workshop(data);
  workshop.save();
})
